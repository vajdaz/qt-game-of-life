#ifndef CELL_H
#define CELL_H

#include <vector>

class Cell
{
public:
    explicit Cell(bool alive = false);
    void kill();
    void revive();
    bool isAlive() const;
    void advance();
    void addNeighbour(Cell* neighbour);
    std::vector<Cell*>::const_iterator getFirstNeighbour() const;
    std::vector<Cell*>::const_iterator getLastNeighbour() const;

private:
    bool alive;
    bool nextAlive;
    std::vector<Cell*> neighbours;
};

using cell_matrix = std::vector<std::vector<Cell> >;

#endif // CELL_H
