#include "QTimer"

#include "gameoflifeapp.h"
#include "gameoflifelogic.h"
#include "ui_gameoflifeapp.h"

GameOfLifeApp::GameOfLifeApp(GameOfLifeLogic& logic, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::GameOfLifeApp),
    timerId(0),
    logic(logic)
{
    ui->setupUi(this);
    ui->renderWidget->setLogic(logic);
    QTimer::singleShot(0, this, SLOT(appReady()));
}

void GameOfLifeApp::appReady()
{
    on_restartButton_clicked();
    on_startStopButton_clicked();
}

GameOfLifeApp::~GameOfLifeApp()
{
    delete ui;
}

void GameOfLifeApp::timerEvent(QTimerEvent* /* event */)
{
    logic.recalculate();
    ui->renderWidget->repaint();
}

void GameOfLifeApp::on_restartButton_clicked()
{
    int numOfRows = ui->renderWidget->getRows();
    int numOfColumns = ui->renderWidget->getColumns();
    logic.initCellMatrix(numOfRows, numOfColumns);
    logic.randomize();
    ui->renderWidget->repaint();
}

void GameOfLifeApp::on_startStopButton_clicked()
{
    if (timerId != 0)
    {
       killTimer(timerId);
       timerId = 0;
       ui->startStopButton->setText("Start");
    }
    else
    {
        timerId = startTimer(200);
        ui->startStopButton->setText("Stop");
    }
}

void GameOfLifeApp::on_clearButton_clicked()
{
    logic.clear();
    ui->renderWidget->repaint();
}
