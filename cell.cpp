#include "cell.h"

Cell::Cell(bool alive /*= false*/) : alive(alive), nextAlive(alive)
{

}

void Cell::kill()
{
    nextAlive = false;
}

void Cell::revive()
{
    nextAlive = true;
}

bool Cell::isAlive() const
{
    return alive;
}

void Cell::advance()
{
    alive = nextAlive;
}

void Cell::addNeighbour(Cell* neighbour)
{
    neighbours.push_back(neighbour);
}

std::vector<Cell*>::const_iterator Cell::getFirstNeighbour() const
{
    return neighbours.cbegin();
}

std::vector<Cell*>::const_iterator Cell::getLastNeighbour() const
{
    return neighbours.cend();
}
