#include "gameoflifeapp.h"
#include "gameoflifelogimpl.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    GameOfLifeLogicImpl logic;
    GameOfLifeApp w(logic);
    w.show();

    return QApplication::exec();
}
