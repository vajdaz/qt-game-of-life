#include <vector>
#include <random>
#include <functional>

#include "gameoflifelogimpl.h"

GameOfLifeLogicImpl::GameOfLifeLogicImpl() :
    numOfRows(0),
    numOfColumns(0),
    cells(numOfRows, std::vector<Cell>(numOfColumns)),
    rule(std::make_unique<AnyRule>(std::vector<int>({2, 3}), std::vector<int>({3})))
{

}

void GameOfLifeLogicImpl::initCellMatrix(int numOfRows, int numOfColumns)
{
    cells = cell_matrix(numOfRows, std::vector<Cell>(numOfColumns));

    for (int row = 0; row < numOfRows; ++row) {
        int over = (row+numOfRows-1) % numOfRows;
        int under = (row+1) % numOfRows;
        for (int col = 0; col < numOfColumns; ++col) {
            int left = (col+numOfColumns-1) % numOfColumns;
            int right = (col+1) % numOfColumns;
            Cell &cell = cells[row][col];
            cell.addNeighbour(&cells[under][col]);
            cell.addNeighbour(&cells[under][left]);
            cell.addNeighbour(&cells[under][right]);
            cell.addNeighbour(&cells[over][col]);
            cell.addNeighbour(&cells[over][left]);
            cell.addNeighbour(&cells[over][right]);
            cell.addNeighbour(&cells[row][right]);
            cell.addNeighbour(&cells[row][left]);
        }
    }
}

void GameOfLifeLogicImpl::recalculate()
{
    for(auto &row : cells)
    {
        for (Cell &cell : row)
        {
            rule->apply(cell);
        }
    }
    for (auto &row : cells)
    {
        for(Cell &cell : row) {
            cell.advance();
        }
    }
}

void GameOfLifeLogicImpl::randomize()
{
    std::default_random_engine engine;
    engine.seed(time(nullptr));
    auto nextRnd = std::bind(std::uniform_int_distribution<>(0,6), engine);

    for (auto &row : cells) {
        for (Cell &cell : row)
        {
            if (nextRnd() < 2)
            {
                cell.revive();
            }
            else
            {
                cell.kill();
            }
            cell.advance();
        }
    }
}

void GameOfLifeLogicImpl::clear()
{
    for (auto &row : cells) {
        for (Cell &cell : row)
        {
            cell.kill();
            cell.advance();
        }
    }
}

cell_matrix& GameOfLifeLogicImpl::getCells() {
    return cells;
}
