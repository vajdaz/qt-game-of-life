#ifndef GAMEOFLIFELOGIMPL_H
#define GAMEOFLIFELOGIMPL_H

#include <memory>

#include "cell.h"
#include "rules.h"
#include "gameoflifelogic.h"

class GameOfLifeLogicImpl : public GameOfLifeLogic
{
public:
    GameOfLifeLogicImpl();
    void initCellMatrix(int numOfRows, int numOfColumns) override;
    void recalculate() override;
    void randomize() override;
    void clear() override;
    cell_matrix& getCells() override;
private:
    int numOfRows;
    int numOfColumns;
    cell_matrix cells;
    std::unique_ptr<GOLRule> rule;
};

#endif // GAMEOFLIFELOGIMPL_H
