#include "QMouseEvent"
#include "QPaintEvent"
#include "QPainter"
#include "QResizeEvent"

#include "cellmatrixrenderwidget.h"

CellMatrixRenderWidget::CellMatrixRenderWidget(QWidget *parent) : QWidget(parent), numOfColumns(0), numOfRows(0), logic(nullptr)
{
    setBackgroundRole(QPalette::Base);
    setAutoFillBackground(true);
}

void CellMatrixRenderWidget::paintEvent(QPaintEvent */*event*/)
{
    QPainter painter(this);
    int zeroOffset = (size().width() - numOfColumns * pixelSize) / 2;
    if (logic != nullptr)
    {
        for (size_t row = 0; row < logic->getCells().size(); ++row) {
            for (size_t col = 0; col < (logic->getCells())[row].size(); ++col) {
                if ((logic->getCells())[row][col].isAlive())
                {
                    painter.fillRect(zeroOffset + col * pixelSize, zeroOffset + row * pixelSize, pixelSize, pixelSize, Qt::GlobalColor::black);
                }
            }
        }
    }
}

void CellMatrixRenderWidget::resizeEvent(QResizeEvent *event)
{
    numOfColumns = event->size().width()/pixelSize;
    numOfRows = event->size().height()/pixelSize;
}

int CellMatrixRenderWidget::getRows()
{
    return numOfRows;
}

int CellMatrixRenderWidget::getColumns()
{
    return numOfColumns;
}

void CellMatrixRenderWidget::mousePressEvent(QMouseEvent *event)
{
    int zeroOffset = (size().width() - numOfColumns * pixelSize) / 2;
    int row = (event->pos().y() - zeroOffset) / pixelSize;
    int col = (event->pos().x() - zeroOffset) / pixelSize;
    Cell &cell = (logic->getCells())[row][col];
    if (cell.isAlive())
    {
        cell.kill();
    }
    else
    {
        cell.revive();
    }
    cell.advance();
    repaint();
}

void CellMatrixRenderWidget::setLogic(GameOfLifeLogic &logic)
{
    this->logic = &logic;
}
