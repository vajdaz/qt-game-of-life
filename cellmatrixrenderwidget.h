#ifndef CELLMATRIXRENDERWIDGET_H
#define CELLMATRIXRENDERWIDGET_H

#include <QWidget>
#include "gameoflifelogic.h"

class CellMatrixRenderWidget : public QWidget
{
    Q_OBJECT
public:
    explicit CellMatrixRenderWidget(QWidget *parent = nullptr);
    void setLogic(GameOfLifeLogic &logic);
    int getRows();
    int getColumns();

protected:
    virtual void paintEvent(QPaintEvent *event);
    virtual void resizeEvent(QResizeEvent *event);
    virtual void mousePressEvent(QMouseEvent *event);

private:
    static const int pixelSize = 10;
    int numOfColumns;
    int numOfRows;
    GameOfLifeLogic *logic;

signals:

public slots:
};

#endif // CELLMATRIXRENDERWIDGET_H
