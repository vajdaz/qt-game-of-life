#ifndef RULES_H
#define RULES_H

#include "cell.h"

class GOLRule
{
public:
    GOLRule() = default;
    virtual ~GOLRule() = default;
    virtual void apply(Cell &cell) = 0;
};

class ClassicRule : public GOLRule
{
public:
    void apply(Cell &cell) override;
};

class AnyRule : public GOLRule
{
public:
    AnyRule(std::vector<int> survive, std::vector<int> reborn);
    void apply(Cell &cell) override;

private:
    std::vector<int> survive;
    std::vector<int> reborn;
};

#endif // RULES_H
