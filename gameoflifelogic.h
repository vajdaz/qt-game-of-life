#ifndef GAMEOFLIFELOGIC_H
#define GAMEOFLIFELOGIC_H

#include "cell.h"

class GameOfLifeLogic
{
public:
    virtual ~GameOfLifeLogic() {};
    virtual void initCellMatrix(int numOfRows, int numOfColumns) = 0;
    virtual void recalculate() = 0;
    virtual void randomize() = 0;
    virtual void clear() = 0;
    virtual cell_matrix& getCells() = 0;
};

#endif // GAMEOFLIFELOGIC_H
