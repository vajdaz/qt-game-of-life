#include <algorithm>

#include "rules.h"

void ClassicRule::apply(Cell &cell)
{
    int livingNeighbours = std::count_if(cell.getFirstNeighbour(), cell.getLastNeighbour(), [](Cell* neighbour){return neighbour->isAlive();});
    if (cell.isAlive())
    {
        if (livingNeighbours < 2 || livingNeighbours > 3)
        {
            cell.kill();
        }
    }
    else
    {
        if (livingNeighbours == 3)
        {
            cell.revive();
        }
    }
}

AnyRule::AnyRule(std::vector<int> survive, std::vector<int> reborn) : survive(std::move(survive)), reborn(std::move(reborn))
{
}

void AnyRule::apply(Cell &cell)
{
    int livingNeighbours = std::count_if(cell.getFirstNeighbour(), cell.getLastNeighbour(), [](Cell* neighbour){return neighbour->isAlive();});
    if (std::any_of(reborn.cbegin(), reborn.cend(), [livingNeighbours](int rebornIf){return rebornIf == livingNeighbours;}))
    {
        cell.revive();
    }
    else if (std::any_of(survive.cbegin(), survive.cend(), [livingNeighbours](int surviveIf){return surviveIf == livingNeighbours;}))
    {
        // do nothing, cell survives
    }
    else
    {
        cell.kill();
    }
}
