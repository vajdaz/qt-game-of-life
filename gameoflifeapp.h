#ifndef GAMEOFLIFEAPP_H
#define GAMEOFLIFEAPP_H
#include <memory>
#include <QDialog>
#include "gameoflifelogic.h"

namespace Ui {
class GameOfLifeApp;
}

class GameOfLifeApp : public QDialog
{
    Q_OBJECT

public:
    explicit GameOfLifeApp(GameOfLifeLogic& logic, QWidget *parent = 0);
    ~GameOfLifeApp();

protected:
    virtual void timerEvent(QTimerEvent *event);

private slots:
    void on_restartButton_clicked();
    void on_startStopButton_clicked();
    void on_clearButton_clicked();
    void appReady();

private:
    Ui::GameOfLifeApp *ui;
    int timerId;
    GameOfLifeLogic& logic;
};

#endif // GAMEOFLIFEAPP_H
